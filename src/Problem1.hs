module Problem1 where

{- https://projecteuler.net/problem=1

If we list all the natural numbers below 10 that are multiples of 3 or 5,
we get 3, 5, 6 and 9. The sum of these multiples is 23.
  Find the sum of all the multiples of 3 or 5 below 1000.
-}
multiples :: Int -> Int -> [Int] -> [Int]
multiples x y list = [i | i <- list, i `mod` x == 0 || i `mod` y == 0]

answer :: Int
answer = sum $ multiples 5 3 [1 .. 999]

sumFactors :: [Int] -> Int
sumFactors [] = 0
sumFactors (x:xs)
  | x `mod` 3 == 0 = x + sumFactors xs
  | x `mod` 5 == 0 = x + sumFactors xs
  | otherwise = sumFactors xs

answer' :: Int
answer' = sumFactors [1 .. 999]
