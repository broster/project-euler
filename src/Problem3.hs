module Problem3 where

import           Data.List

{- https://projecteuler.net/problem=3

The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
-}
largestPrimeFactor :: Int -> Int
largestPrimeFactor n
  | smallest_factor == n = n
  | otherwise = largestPrimeFactor largest_factor
  where
    smallest_factor = smallestPrimeFactor n
    largest_factor = n `div` smallest_factor

smallestPrimeFactor :: Int -> Int
smallestPrimeFactor n = smallestFactor n (2 : [3,5 ..])
  where
    smallestFactor n (x:xs)
      | n == 1 = 1
      | n `mod` x == 0 = x
      | otherwise = smallestFactor n xs
